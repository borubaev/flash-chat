import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class FlashChatText extends StatelessWidget {
  const FlashChatText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: Theme.of(context)
          .textTheme
          .headlineLarge!
          .copyWith(color: Colors.black, fontSize: 54),
      child: AnimatedTextKit(
        totalRepeatCount: 1000,
        animatedTexts: [
          WavyAnimatedText('Flash chat'),
        ],
        isRepeatingAnimation: true,
      ),
    );
  }
}
