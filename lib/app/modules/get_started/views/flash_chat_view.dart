import 'package:flash_chat/app/components/animation_logo.dart';
import 'package:flash_chat/app/components/custom_button.dart';
import 'package:flash_chat/app/components/flash_chat.dart';
import 'package:flash_chat/app/modules/get_started/controllers/flash_chat_controller.dart';
import 'package:flash_chat/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class FlashChatView extends GetView<FlashChatController> {
  const FlashChatView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimationLogo(),
                FlashChatText(),
              ],
            ),
            const SizedBox(
              height: 14,
            ),
            CustomButton(
              text: 'Login',
              color: Colors.blue.shade400,
              onpressed: () => Get.toNamed(Routes.AUTH, arguments: true),
            ),
            const SizedBox(
              height: 24,
            ),
            CustomButton(
              text: 'Register',
              color: Colors.blue.shade700,
              onpressed: () => Get.toNamed(Routes.AUTH, arguments: false),
            ),
          ],
        ),
      ),
    );
  }
}
