import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../services/service_manager.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  HomeView({Key? key}) : super(key: key);
  // final String? uid;
  final isAuth = userManager.init();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeView'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'HomeView is working',
              style: TextStyle(fontSize: 20),
            ),
            // Text(
            //   'ssd',
            //   style: TextStyle(fontSize: 20),
            // ),
            ElevatedButton(
                onPressed: () {
                  controller.onInit();
                },
                child: Text('Log out'))
          ],
        ),
      ),
    );
  }
}
