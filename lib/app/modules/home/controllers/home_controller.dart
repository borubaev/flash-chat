import 'package:flash_chat/services/service_manager.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';

class HomeController extends GetxController {
  @override
  void onInit() {
    Get.offAllNamed(Routes.FLASH_CHAT);
  }
}
