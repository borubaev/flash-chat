import 'package:flash_chat/app/routes/app_pages.dart';
import 'package:flash_chat/services/service_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../service/login_register.dart';

class AuthController extends GetxController {
  final email = TextEditingController();
  final password = TextEditingController();
  final formkey = GlobalKey<FormState>();

  Future<void> registerLogin(bool isLogin) async {
    if (formkey.currentState!.validate() &&
        GetUtils.isEmail(email.text) &&
        GetUtils.isPassport(password.text)) {
      Get.defaultDialog(content: const CupertinoActivityIndicator());
      final user = isLogin
          ? await LoginService.login(email.text, password.text)
          : await LoginService.register(email.text, password.text);
      Get.back();

      if (user != null) {
        await userManager.setUid(user.user!.uid);
        await Get.offAllNamed(Routes.HOME);
      } else {
        Get.defaultDialog(
            title: isLogin
                ? 'You have entered an incorrect password or login'
                : 'Error',
            cancel: Text('Cancel'));
      }
    } else {
      Get.showSnackbar(const GetSnackBar(
        message: 'Fill out the form',
      ));
    }
  }
}
